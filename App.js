import { useState } from "react";


function App() {
  const [val, setVal] = useState("");

  const tts = () => {
    // js標準のAPI機能のfetchを使う。
    // bodyはjson形式で。
    fetch('http://localhost/tts', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({text: val})
    })
      // responseのデータはBlob形式で取得する。
      .then((response) => response.blob())
      .then((data) => {
        // 取得してBlob形式にした音声データは、クライアントのメモリに保存される。
        // アクセス出来るようにURLを生成する。
        const url = (window.URL || window.webkitURL).createObjectURL(data)
        let audio = new Audio(url);
        audio.onended = () => {
          // 音声データで使用したメモリの開放をする。
          audio = null;
        }
        audio.play();
        console.log('audio play...')
      })
      .catch((error) => {
        console.error('Error:', error);
      })
  };

  return (
    <div className="App">
      <header className="App-header">
        <textarea
          id="textarea"
          value={val}
          onInput={e => setVal(e.target.value)}
        />
        <button
          style={{width: "75px", height: "25px"}}
          type="button"
          onClick={() => tts()}
        >
          Let's tts !!
        </button>
      </header>
    </div>
  );
}

export default App;
