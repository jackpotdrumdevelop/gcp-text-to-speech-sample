from fastapi import FastAPI, Response
from google.cloud import texttospeech
from pydantic import BaseModel


class Item(BaseModel):
    text: str

app = FastAPI()

@app.post("/tts")
def tts(data: Item):
    # APIを実行するクライアントの作成
    client = texttospeech.TextToSpeechClient()

    # 音声合成したい文字列を渡す。
    # - text: この引数に設定した文字列が音声合成される。
    synthesis_input = texttospeech.SynthesisInput(text=data.text)

    # 言語、声の設定
    # - language_code: 言語を選択する引数。ja-JPで日本語になる。
    # - name: 声質を選択する引数。男性、女性の声を選択できる。
    voice = texttospeech.VoiceSelectionParams(
        language_code="ja-JP",
        name="ja-JP-Neural2-B"
    )

    # 出力される音声の設定
    # - audio_encoding: 音声のエンコード方式。下記はmp3形式の設定。
    audio_config = texttospeech.AudioConfig(
        audio_encoding=texttospeech.AudioEncoding.MP3
    )

    # 音声合成のAPIを実行する。
    # request
    # - input: 上記で作成したSynthesisInputオブジェクトを設定。
    # - voice: 上記で作成したVoiceSelectionParamsオブジェクトを設定。
    # - audio_config: 上記で作成したAudioConfigオブジェクトを設定。
    # response
    # - audio_content: 合成された音声。bytesフォーマットの文字列。
    response = client.synthesize_speech(
        input=synthesis_input,
        voice=voice,
        audio_config=audio_config
    )

    return Response(content=response.audio_content)

