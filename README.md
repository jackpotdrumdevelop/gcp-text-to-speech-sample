# GCP Cloud APIのText-To-Speechのデモ

画面からテキストを入力して送信すると、合成音声が流れる簡単なデモ。

処理の流れは以下の様にした。

```
クライアント(React) --> サーバー(FastAPI) --> Text-To-Speech
```

処理の説明はコメントに。

## Text-To-Speechを使う前準備

GCPの画面からサービスアカウントを作成し、認証情報をjsonファイルとしてダウンロードしておく必要がある。jsonファイルのダウンロードと認証方法は下記ドキュメントを参考にする。

https://cloud.google.com/docs/authentication/production?hl=ja#auth-cloud-explicit-python
